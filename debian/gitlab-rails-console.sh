#! /bin/sh

set -e

# Read debian specific configuration
. /etc/gitlab-common/gitlab-common.conf
. /etc/gitlab/gitlab-debian.conf
export DB RAILS_ENV

cd /usr/share/gitlab

runuser -u ${gitlab_user} -- sh -c "/usr/bin/bundle exec rails console -e production"
